from django.urls import path, include
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index_view, name='index'),
    path('add_status', views.add_status_view, name="add_status"),
    path('del_status/<int:id>', views.del_status_view, name='del_status'),
]
