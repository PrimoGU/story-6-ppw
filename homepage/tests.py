from django.test import TestCase, LiveServerTestCase, Client
from django.urls import resolve
from django.test.client import RequestFactory
from .views import index_view, add_status_view, del_status_view
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_using_index_view_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index_view)

	def test_url_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_landing_page_contains_correct_html(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn('HALO! APA KABAR?', html_response)

	def test_landing_page_does_not_contain_correct_html(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn('I am not supposed to be here', html_response)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status = Status.objects.create( title='Membuat TTD di Django',
											description='Membuat Unit dan Functional Test di Django')
		# Retrieving all available status
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_form_status_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="status-form-input"', form.as_p())
		self.assertIn('id="id_title"', form.as_p())
		self.assertIn('class="status-form-textarea"', form.as_p())
		self.assertIn('id="id_description"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data = {'title': '', 'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['description'], ["This field is required."])

	def test_form_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/add_status', {'title': test, 'description': test})
		self.assertEqual(response_post.status_code, 302)
		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_form_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/add_status', {'title': '', 'description': ''})
		self.assertEqual(response_post.status_code, 302)
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

	def test_can_delete_a_posted_status(self):
		test = 'Anonymous'
		response_post = Client().post('/add_status', {'title': test, 'description': test})
		self.assertEqual(response_post.status_code, 302)

		status_id = Status.objects.only('id').get(title=test).id
		rf = RequestFactory()
		del_status_view(rf, status_id)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class Lab6FunctionalTest(LiveServerTestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
		super(Lab6FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get(self.live_server_url)
		# Find the form element
		title = selenium.find_element_by_id('id_title')
		description = selenium.find_element_by_id('id_description')
		submit = selenium.find_element_by_id('submit')
		# Fill the form with data
		title.send_keys('Mengerjakan Story 6 PPW')
		description.send_keys('Tugas lab kali ini adalah tentang Test-Driven Development dengan Django dan Selenium')
		# Submit the form
		submit.send_keys(Keys.RETURN)
