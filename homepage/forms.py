from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Please do not leave this empty!',
    }
    title_attrs = {
        'type': 'text',
        'class': 'status-form-input',
        'placeholder':'Enter new status...'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 8,
        'class': 'status-form-textarea',
        'placeholder':'Enter status description...'
    }

    title = forms.CharField(label='', required=True, max_length=30, widget=forms.TextInput(attrs=title_attrs))
    description = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=description_attrs))
